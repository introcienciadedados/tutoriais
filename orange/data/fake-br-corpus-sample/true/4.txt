ï»¿Doria vai receber ZÃ© Celso apÃ³s reuniÃ£o com representante de Silvio Santos

O prefeito JoÃ£o Doria (PSDB) receberÃ¡ o dramaturgo JosÃ© Celso Martinez CorrÃªa e as arquitetas do Teatro Oficina no dia 29 para discutir o imbrÃ³glio das torres que Silvio Santos quer construir ao lado da sede da companhia. O vereador Eduardo Suplicy (PT), que pediu a reuniÃ£o, e o maestro JoÃ£o Carlos Martins tambÃ©m devem comparecer. Doria recebeu na quinta-feira (4) representantes do Grupo Silvio Santos, o que gerou protestos do outro lado.

TÃ DENTRO
O maestro estÃ¡ entrando para valer na campanha em defesa do Oficina. Ele, que participou do processo de tombamento do teatro quando foi secretÃ¡rio da Cultura do Estado, afirma que estÃ¡ "200% dentro" da causa porque considera ZÃ© Celso "um herÃ³i". Martins farÃ¡ um concerto no local no dia 25, em apoio ao movimento.
