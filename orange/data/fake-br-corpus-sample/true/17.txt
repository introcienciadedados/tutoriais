ï»¿Juiz determina soltura de 4 presos por pensÃ£o alimentÃ­cia no Compaj

Segundo Defensoria, presos nÃ£o foram transferidos para Vidal Pessoa. DPE argumentou que crise expÃµe perigo a devedores de pensÃ£o

O juiz Leoney Figlioulo Harraquian determinou no sÃ¡bado (7) a soltura em carÃ¡ter de urgÃªncia de quatro homens que estavam presos no Complexo PenitenciÃ¡rio AnÃ­sio Jobim (Compaj) por atraso no pagamento de pensÃ£o alimentÃ­cia. A decisÃ£o atendeu pedido da Defensoria PÃºblica do Amazonas (DPE-AM) diante da crise no sistema carcerÃ¡rio em Manaus, agravada com rebeliÃµes, fuga e mortes desde a Ãºltima semana. De acordo com a DPE, os presos nÃ£o fazem parte do grupo transferido para a Cadeia Vidal Pessoa.

No pedido, a DPE-AM argumentou que a crise expÃµe perigo excessivo aos devedores de pensÃ£o alimentÃ­cia que cumprem medidas coercitivas no regime fechado lado a lado com os condenados por crimes comuns.

âNesse sentido, a prisÃ£o civil deve sofrer gradativos sacrifÃ­cios atÃ© a perfeita harmonizaÃ§Ã£o com o direito Ã  vida. Assim, ou bem os presos dever ser realocados para outra unidade segura, como por exemplo a carceragem do Comando da PolÃ­cia Militar ou, em Ãºltimo caso, a substituiÃ§Ã£o por prisÃ£o domiciliar, com ou sem monitoraÃ§Ã£o eletrÃ´nicaâ, diz o pedido de tutela provisÃ³ria antecipada de urgÃªncia antecedente da DPE-AM.

A Defensoria PÃºblica pediu que, caso nÃ£o houvesse alternativa para manutenÃ§Ã£o do enceramento de forma segura, que os presos por atraso no pagamento de pensÃ£o alimentÃ­cia fossem liberados com o compromisso de apresentar a quitaÃ§Ã£o do dÃ©bito com o alimentado em prazo estabelecido pela JustiÃ§a.

O juiz Leoney Figlioulo deu prazo de 30 dias para a apresentaÃ§Ã£o do comprovante de quitaÃ§Ã£o da dÃ­vida da pensÃ£o alimentÃ­cia, sob pena de renovaÃ§Ã£o da prisÃ£o, e determinou a expediÃ§Ã£o do alvarÃ¡ de soltura para AndrÃ© da Silva Moraes, Francival de Almeida Silva, Thiago Correa da Costa e Valdemar Torres de Souza Neto.

Juiz determina soltura de 4 presos por pensÃ£o alimentÃ­cia no Compaj
Segundo Defensoria, presos nÃ£o foram transferidos para Vidal Pessoa.
DPE argumentou que crise expÃµe perigo a devedores de pensÃ£o
Do G1 AM

FACEBOOK
O juiz Leoney Figlioulo Harraquian determinou no sÃ¡bado (7) a soltura em carÃ¡ter de urgÃªncia de quatro homens que estavam presos no Complexo PenitenciÃ¡rio AnÃ­sio Jobim (Compaj) por atraso no pagamento de pensÃ£o alimentÃ­cia. A decisÃ£o atendeu pedido da Defensoria PÃºblica do Amazonas (DPE-AM) diante da crise no sistema carcerÃ¡rio em Manaus, agravada com rebeliÃµes, fuga e mortes desde a Ãºltima semana. De acordo com a DPE, os presos nÃ£o fazem parte do grupo transferido para a Cadeia Vidal Pessoa.

No pedido, a DPE-AM argumentou que a crise expÃµe perigo excessivo aos devedores de pensÃ£o alimentÃ­cia que cumprem medidas coercitivas no regime fechado lado a lado com os condenados por crimes comuns.

âNesse sentido, a prisÃ£o civil deve sofrer gradativos sacrifÃ­cios atÃ© a perfeita harmonizaÃ§Ã£o com o direito Ã  vida. Assim, ou bem os presos dever ser realocados para outra unidade segura, como por exemplo a carceragem do Comando da PolÃ­cia Militar ou, em Ãºltimo caso, a substituiÃ§Ã£o por prisÃ£o domiciliar, com ou sem monitoraÃ§Ã£o eletrÃ´nicaâ, diz o pedido de tutela provisÃ³ria antecipada de urgÃªncia antecedente da DPE-AM.

A Defensoria PÃºblica pediu que, caso nÃ£o houvesse alternativa para manutenÃ§Ã£o do enceramento de forma segura, que os presos por atraso no pagamento de pensÃ£o alimentÃ­cia fossem liberados com o compromisso de apresentar a quitaÃ§Ã£o do dÃ©bito com o alimentado em prazo estabelecido pela JustiÃ§a.

O juiz Leoney Figlioulo deu prazo de 30 dias para a apresentaÃ§Ã£o do comprovante de quitaÃ§Ã£o da dÃ­vida da pensÃ£o alimentÃ­cia, sob pena de renovaÃ§Ã£o da prisÃ£o, e determinou a expediÃ§Ã£o do alvarÃ¡ de soltura para AndrÃ© da Silva Moraes, Francival de Almeida Silva, Thiago Correa da Costa e Valdemar Torres de Souza Neto.

Entenda o caso
O primeiro tumulto nas unidades prisionais do estado ocorreu no Instituto Penal AntÃ´nio Trindade (Ipat), localizado no km 8 da BR-174 (Manaus-Boa Vista). Um total de 72 presos fugiu da unidade prisional na manhÃ£ de domingo (1o).

Horas mais tarde, por volta de 14h, detentos do Compaj iniciaram uma rebeliÃ£o violenta na unidade, que resultou na morte de 56 presos. O massacre foi liderado por internos da facÃ§Ã£o FamÃ­lia do Norte (FDN).

A rebeliÃ£o no Compaj durou aproximadamente 17h e acabou na manhÃ£ desta segunda-feira (2). ApÃ³s o fim do tumulto na unidade, o Ipat e o Centro de DetenÃ§Ã£o ProvisÃ³ria Masculino (CDPM) tambÃ©m registraram distÃºrbios.