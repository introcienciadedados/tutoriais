# Exemplo de dashboard com Streamlit

Veja a documentação do Streamlit no [site do projeto](https://docs.streamlit.io/)

## Instalação do streamlit:

```
pip install streamlit
```

## Executar a app:

```
streamlit run app.py
```


